Name:           libcds
Version:        2.3.3
Release:        3%{?dist}
Summary:        A C++ library of Concurrent Data Structures

License:        Boost Software License 1.0
URL:            https://github.com/khizmax/libcds
Source0:        https://github.com/khizmax/libcds/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz

Patch0:         LibSuffix.patch

BuildRequires:  gcc-c++
BuildRequires:  cmake

%description
The Concurrent Data Structures (CDS) library is a collection of concurrent
containers that don't require external (manual) synchronization for shared
access, and safe memory reclamation (SMR) algorithms like Hazard Pointer
and user-space RCU that is used as an epoch-based SMR.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
This package contains the header file for using %{name}.

%package        static
Summary:        Header only development files for %{name}
Requires:       %{name}-devel%{?_isa} = %{version}-%{release}

%description    static
This package contains the files for using %{name} as a header only library.

%prep
%autosetup

%build
%cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo \
%if 0%{?rhel} > 8 || 0%{?fedora}
..
%endif
%cmake_build

%install
%cmake_install

%files
%{_libdir}/libcds.so.*

%files devel
%{_includedir}/cds/
%{_libdir}/libcds.so
%{_libdir}/cmake/LibCDS/

%files static
%{_libdir}/libcds-s.a

%changelog
* Fri Feb 18 2022 Milivoje Legenovic <milivoje.legenovic@profidata.com> - 2.3.3-3
- xdev-6 build

* Sat Jun 26 2021 Levent Demirörs <levent.demiroers@profidata.com> - 2.3.3-2
- Upgrade to GCC 10
